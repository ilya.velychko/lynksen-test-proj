import React from 'react';
import './App.css';
import { CatsSlider } from "./components";
import { useFetch } from "./hooks";

function App() {
    const data = useFetch();

    if (!data) {
        return <>
            No Data Found...
        </>
    }

    return (
        <div className="wrapper">
            <CatsSlider slides={data} />
        </div>
    );
}

export default App;
