export interface IBreeds {
    id: string;
    name: string;
    description: string
}

export interface ICatsSlider {
    height: number;
    width: number;
    id: string;
    url: string;
    breeds: IBreeds[]
}