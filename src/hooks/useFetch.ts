import useSWR from "swr";

const API_KEY = 'live_Q0L7nk5Px1teOINKkjGY5k0RpgaZvZiinQaEqf31lOCd2EQ73wK3xJNnWZGGZpdk';
export function useFetch() {
    const { data } = useSWR(
        `https://api.thecatapi.com/v1/images/search?limit=10&breed_ids=acur,asho,awir,amau,amis`,
        (url: any) => fetch(url, { headers: { 'x-api-key': API_KEY } }).then(response => response.json()),
        {
            refreshInterval: 0,
            revalidateOnFocus: false,
            revalidateOnMount: false
        }
    );

    return data;
}