import React, { useState } from "react";
import { ShowMore } from "./index";
import { ICatsSlider } from "../../interfaces";
import './cats-sliders-styles.css';

export function CatsSlider({ slides }: { slides: ICatsSlider[] }) {
    const [currentIndex, setCurrentIndex] = useState<number>(0);

    const goToPrevious = () => {
        const isFirstSlide = currentIndex === 0;
        const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
        setCurrentIndex(newIndex);
    }

    const goToNext = () => {
        const isLastSlide = currentIndex === slides.length - 1;
        const newIndex = isLastSlide ? 0 : currentIndex + 1;
        setCurrentIndex(newIndex);
    }

    return (
        <div className="slider-wrapper">
            <div className="slider-image-wrapper">
                <img src={slides[currentIndex].url} alt="cat" />
                <div className="slider-controllers">
                    <button onClick={goToPrevious} className="btn">Previous Cat</button>
                    <button onClick={goToNext} className="btn">Next Cat</button>
                </div>
            </div>
            <div className="cat-info-list">
                {
                    slides[currentIndex].breeds.map((breed) => (
                        <div key={breed.id}>
                            <span className="cat-info-breed">
                                <em>Breed</em>: {breed.name}
                            </span>
                            <div className="separator" />
                            <span className="cat-info-description">
                                <em>Description</em>:
                                <ShowMore value={breed.description} />
                            </span>
                        </div>
                    ))
                }
            </div>
        </div>
    )
}