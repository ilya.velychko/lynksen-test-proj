import './cats-sliders-styles.css';
import { useState } from "react";

export function ShowMore({ value }: { value: string }) {
    const [showMore, setShowMore] = useState<boolean>(false);

    return (
        <>
            <span>
                {showMore ? value : value.slice(0, 50).concat('...')}
            </span>
            <br/>
            <span
                className="show-more"
                onClick={() => setShowMore((prevState) => !prevState)}
            >
                Show { showMore ? 'Less' : 'More' }
            </span>
        </>
    )
}